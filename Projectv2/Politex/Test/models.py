from django.db import models

class Question( models.Model ):
    text_quest = models.CharField ( max_length = 150, verbose_name="quest")
    answer_1 = models.CharField ( max_length = 20, verbose_name="an1")
    answer_2 = models.CharField ( max_length = 20, verbose_name="an2")
    answer_3 = models.CharField ( max_length = 20, verbose_name="an3")
